using UnityEngine;

namespace Gtion.Plugin.CustomInspector.ReadOnly
{

    /// <summary>
    /// Read Only attribute.
    /// Attribute is use only to mark ReadOnly properties.
    /// </summary>
    public class ReadOnlyAttribute : PropertyAttribute { }
}
