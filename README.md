# GDC Json-Tools
## Built In
![Build Status](https://badgen.net/badge/Build/Passing/green) ![Version](https://badgen.net/badge/Release/v1.1.0/blue) ![Languages](https://badgen.net/badge/Languages/C-Sharp/blue)
![Engine](https://badgen.net/badge/Engine/Unity%202020.3.4f1/gray)

Depedencies
- no depedencies

## Installation
Please check documentation below
> https://docs.unity3d.com/2019.3/Documentation/Manual/upm-ui-giturl.html

## What is it?
Custom Inspector tools similar to odin!

## v2.0.0
- [New] EnumArray
- [New] ExecutionOrder
- [New] RequireInterface (or ArrayInterface)

## v1.1.0 Fix bug! 
- Can't build Android

## Feature List
- ReadOnly
- Button
- EnumArray
- ExecutionOrder
- RequireInterface (or ArrayInterface)

## Usage

#### ReadOnly

```cs
using Gtion.Plugin.CustomInspector.ReadOnly;

public class NewScript
{
    [ReadOnly] // use like this
    public string thisIsString = "Try";
}
```

#### Button

```cs
using Gtion.Plugin.CustomInspector.Button;

public class NewScript
{
    [Button]
    public void AFunction()
    {

    }
}
```

#### EnumArray

```cs
using Gtion.Plugin.CustomInspector.EnumArray;

public enum Direction
{
    Left,
    Right,
    Top,
    Down
}

public class NewScript
{
    [EnumNamedArray(typeof(Direction))]
    public int[] DirectionValue;
}
```


#### ExecutionOrder

```cs
using Gtion.Plugin.CustomInspector.ExecutionOrder;

[ExecutionOrder(-1)]
public class NewScript
{
    //...
}
```


#### RequireInterface

```cs
using Gtion.Plugin.CustomInspector.FieldInterface;

public interface ITest
{
    //...
}

public class NewScript
{
    [RequireInterface(typeof(ITest))]
    List<Object> temp;
}
```



## Contributor


| Profile | 
| ------ |
| [![Firdiar](https://gitlab.com/uploads/-/system/user/avatar/2307294/avatar.png?raw=true)](https://www.linkedin.com/in/firdiar) |
| [Firdiansyah Ramadhan](https://www.linkedin.com/in/firdiar) | 

## License

MIT

**Free Software, Hell Yeah!**
